console.log("entro ")

//permitir, cambia y reasigna nombre, vive dentro scoope
let firstName = 'nombre' 

const lastName = 'holas'

var isDeveloper = false


console.log("firstName ",firstName)
firstName = "nombre2"
console.log("firstName ",firstName)

//error
// console.log("lastName ",lastName)
// lastName = "nombre2"
// console.log("lastName ",lastName)

//no se puede mutar
console.log("no se puede mutar ",lastName.toLowerCase())
console.log("no se puede mutar ",lastName)

//function expreccion a una constante se le
//asigna una funcion
const sumar = (op1, op2) => {
    return op1 + op2
}

//primera detecta las funciones
function restar(a,b) {
    return a - b
}

console.log(sumar(4,4))